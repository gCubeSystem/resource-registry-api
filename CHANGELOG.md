This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry API

# [v6.0.0-SNAPSHOT]

- Migrating code to comply with Smartgears 4


# [v5.1.0-SNAPSHOT]

- Added Contexts as Teee in ContextCache [#24555] 
- Added query parameters to paginate result of queries [#24648]


## [v5.0.0]

- Migrate code to reorganized E/R format [#24992]
- Added query parameters to request Metadata [#25040]


## [v4.3.0]

- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)


## [v4.2.0]

- Added INCLUDE_RELATION_PARAM param in AccessPath used by query APIs [#20298]
- Added support for context names included in header among UUIDs [#22090]
- Added support for Json Query [#22047]
- Added support for Query Templates [#22091]
- Added class to get service URL [#23658]


## [v4.1.0] 

- Added ContextCache utilities 
- Added support to provide APIs to get the list of instance contexts [#20012] [#20013]


## [v4.0.0] [r4.26.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19116]


## [v3.0.0] [r4.21.0] - 2020-03-30

- Refactored code to support renaming of Embedded class to Property [#13274]
- Removed code which is now part of gxREST [#11455]
- Reorganized APIs due to IS Model Reorganization


## [v2.0.0] [r4.13.0] - 2018-11-20

- Redesigned REST interface [#11288]


## [v1.7.0] [r4.9.0] - 2017-12-20

- Added SchemaAlreadyPresentException [#10207]
- Added Utility to get the Type name from an ER Class or interface [#10172]
- Changed pom.xml to use new make-servicearchive directive [#10162]
- Forced UTF-8 HTTP body encoding in HTTPCall class [#9966]


## [v1.6.0] [4.7.0] - 2017-10-09

- Added an API to retrieve Resource instances filtering them [#9772]


## [v1.5.0] [r4.6.0] - 2017-07-25

- Added HTTPS support [#8757]
- Added HTTP Redirection support [#8757]


## [v1.4.0] [r4.5.0] - 2017-06-07

- Added ERAvalailableInanotherContext exception and its hierarchy to improve context management of ERs
- Improved HTTPCall class to support HEAD methods and Exception managements in such a case


## [v1.3.0] [r4.3.0] - 2017-03-16

- Added HTTP utilities for clients implementations
- Added ResourceRegistryException JSON serialization


## [v1.2.0] [r4.2.0] - 2016-12-15

- Added API AddToContext
- Added API to support update Resource (with all facets)
- Reorganized Exceptions


## [v1.1.0] [r4.1.0] - 2016-11-07

- Improved code quality


## [v1.0.0] [r4.0.0] - 2016-07-28

- First Release

