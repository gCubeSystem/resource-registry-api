package org.gcube.informationsystem.resourceregistry.api.rest;

import org.gcube.informationsystem.resourceregistry.api.ContextTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ServiceInstanceTest extends ContextTest {

	@Test
	public void test() {
		String url = ServiceInstance.getServiceURL();
		String testURL = ServiceInstance.BASE_URL.replace("url.", "url.gcube.");
		Assert.assertTrue(url.compareTo(testURL)==0);
	}
	
}
