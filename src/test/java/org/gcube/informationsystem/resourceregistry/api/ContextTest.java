/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.AccessTokenSecret;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ContextTest.class);
	
	protected static final String CONFIG_INI_FILENAME = "config.ini";
	
	public static final String PARENT_DEFAULT_TEST_SCOPE;
	public static final String DEFAULT_TEST_SCOPE;
	public static final String ALTERNATIVE_TEST_SCOPE;
	
	public static final String GCUBE;
	public static final String DEVNEXT;
	public static final String NEXTNEXT;
	public static final String DEVSEC;
	public static final String DEVVRE;
		
	protected static final Properties properties;
	
	protected static final String CLIENT_ID_PROPERTY_KEY = "client_id"; 
	protected static final String CLIENT_SECRET_PROPERTY_KEY = "client_secret";
	
	protected static final String clientID; 
	protected static final String clientSecret;
	
	static {
		GCUBE = "/gcube";
		DEVNEXT = GCUBE + "/devNext";
		NEXTNEXT = DEVNEXT + "/NextNext";
		DEVSEC = GCUBE + "/devsec";
		DEVVRE = DEVSEC + "/devVRE";
		
		PARENT_DEFAULT_TEST_SCOPE = "/gcube";
		DEFAULT_TEST_SCOPE = DEVNEXT;
		ALTERNATIVE_TEST_SCOPE = NEXTNEXT;
		
		properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(CONFIG_INI_FILENAME);
		try {
			// load the properties file
			properties.load(input);
			
			clientID = properties.getProperty(CLIENT_ID_PROPERTY_KEY);
			clientSecret = properties.getProperty(CLIENT_SECRET_PROPERTY_KEY);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public static void set(Secret secret) throws Exception {
		SecretManagerProvider.reset();
		SecretManagerProvider.set(secret);
	}
	
	public static void setContextByName(String fullContextName) throws Exception {
		Secret secret = getSecretByContextName(fullContextName);
		set(secret);
	}
	
	private static TokenResponse getJWTAccessToken(String context) throws Exception {
		TokenResponse tr = KeycloakClientFactory.newInstance().queryUMAToken(clientID, clientSecret, context, null);
	    return tr;
	}
	
	public static Secret getSecretByContextName(String context) throws Exception {
		TokenResponse tr = getJWTAccessToken(context);
		Secret secret = new AccessTokenSecret(tr.getAccessToken(), context);
		return secret;
	}
	
	public static String getUser() {
		String user = Metadata.UNKNOWN_USER;
		try {
			user = SecretManagerProvider.get().getOwner().getId();
		} catch(Exception e) {
			logger.error("Unable to retrieve user. {} will be used", user);
		}
		return user;
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(DEFAULT_TEST_SCOPE);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		SecretManagerProvider.reset();
	}
	
}
