package org.gcube.informationsystem.resourceregistry.api.contexts;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.tree.NodeElaborator;
import org.gcube.informationsystem.tree.Tree;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextCacheTest{
	
	private static final Logger logger = LoggerFactory.getLogger(ContextCacheTest.class);
	
	protected File getTypesDirectory() throws Exception {
		URL logbackFileURL = ContextCacheTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "contexts");
	}
	
	protected String readFile(File file) throws IOException {
		byte[] encoded = Files.readAllBytes(file.toPath());
		return new String(encoded, Charset.defaultCharset());
	}
	
	protected List<Context> getContexts() throws Exception {
		File typesDirectory = getTypesDirectory();
		File file = new File(typesDirectory, "contexts.json");
		String json = readFile(file);
		List<Context> contexts = ElementMapper.unmarshalList(Context.class, json);
		return contexts;
	}
	
	@Test
	public void test() throws Exception {
		List<Context> contexts = getContexts();
		ContextCache contextCache = new ContextCache();
		contextCache.setContexts(contexts);
		contextCache.setContextCacheRenewal(new ContextCacheRenewal() {
			@Override
			public List<Context> renew() throws ResourceRegistryException {
				try {
					return getContexts();
				}catch (Exception e) {
					throw new ResourceRegistryException(e);
				}
			}
		});
		Tree<Context> contextTree = contextCache.getContextsTree();
		contextTree.elaborate(new NodeElaborator<Context>() {
			
			@Override
			public void elaborate(Node<Context> node, int level) throws Exception {
				StringBuffer stringBuffer = new StringBuffer();
				for (int i = 0; i < level; ++i) {
					stringBuffer.append(Node.INDENTATION);
				}
				
				Context context = node.getNodeElement();
				UUID uuid = context.getID();
				String fullName = contextCache.getContextFullNameByUUID(uuid);
				logger.info("{}- {} (ID:{})", stringBuffer.toString(), fullName, uuid);
			}
		});
	}	
}
