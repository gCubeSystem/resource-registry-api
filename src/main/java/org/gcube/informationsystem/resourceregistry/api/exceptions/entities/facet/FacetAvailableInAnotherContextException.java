package org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet;

import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.EntityAvailableInAnotherContextException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetAvailableInAnotherContextException extends EntityAvailableInAnotherContextException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7502387344011649559L;

	public FacetAvailableInAnotherContextException(String message) {
		super(message);
	}

	public FacetAvailableInAnotherContextException(Throwable cause) {
		super(cause);
	}
	
	public FacetAvailableInAnotherContextException(String message, Throwable cause) {
		super(message, cause);
	}

}
