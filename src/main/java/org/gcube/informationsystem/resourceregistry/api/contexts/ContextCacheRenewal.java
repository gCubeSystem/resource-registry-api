package org.gcube.informationsystem.resourceregistry.api.contexts;

import java.util.List;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface ContextCacheRenewal {

	public List<Context> renew() throws ResourceRegistryException;
	
}
