package org.gcube.informationsystem.resourceregistry.api.request;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface RequestInfo {

	public Integer getLimit();

	public void setLimit(Integer limit);

	public Integer getOffset();

	public void setOffset(Integer offset);

	public boolean includeMeta();

	public void setIncludeMeta(boolean includeMeta);

	public boolean allMeta();

	public void setAllMeta(boolean allMeta);

	public boolean isHierarchicalMode();

	public void setHierarchicalMode(boolean hierarchicalMode);

	public boolean includeContexts();

	public void setIncludeContexts(boolean includeContexts);

}
