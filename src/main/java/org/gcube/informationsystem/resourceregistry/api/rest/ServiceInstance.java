package org.gcube.informationsystem.resourceregistry.api.rest;

import org.gcube.common.security.providers.SecretManagerProvider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class ServiceInstance {

	private static final String PROD_ROOT_SCOPE = "/d4science.research-infrastructures.eu";
	
	public static final String BASE_URL = "https://url.d4science.org";
	
	public static String getServiceURL() {
		String context = SecretManagerProvider.get().getContext();
		return getServiceURL(context);
	}
	
	public static String getServiceURL(String context) {
		if(context.startsWith(PROD_ROOT_SCOPE)) {
			return BASE_URL;
		}
		String root = context.split("/")[1];
		return BASE_URL.replace("url", "url." + root.replaceAll("\\.", "-"));
	}
	
}
