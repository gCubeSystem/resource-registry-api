package org.gcube.informationsystem.resourceregistry.api.exceptions.types;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SchemaViolationException extends ResourceRegistryException {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -2563703637351710301L;

	public SchemaViolationException(String message) {
		super(message);
	}
	
	public SchemaViolationException(Throwable cause) {
		super(cause);
	}
	
	public SchemaViolationException(String message, Throwable cause) {
		super(message, cause);
	}
}
