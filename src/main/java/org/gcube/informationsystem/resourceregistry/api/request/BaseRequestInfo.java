package org.gcube.informationsystem.resourceregistry.api.request;

import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class BaseRequestInfo implements RequestInfo {
	
	public static final Integer DEFAULT_OFFSET = 0;
	public static final Integer DEFAULT_LIMIT = 10;
	
	public static final Integer UNBOUNDED_LIMIT = -1;
	
	/**
	 * The offset parameter indicates the starting position of the result.
	 */
	protected Integer offset;
	
	/**
	 *  To get unlimited results the limit query parameters must be set to -1. 
	 * 	If the results are too much the operation could have a timeout. 
	 */
	protected Integer limit;
	
	/**
	 * Track if the request requested to include {@link Metadata} 
	 */
	protected boolean includeMeta;
	
	/**
	 * Track if the request requested to include {@link Metadata} in all
	 * {@link IdentifiableElement} or just in the root instance 
	 */
	protected boolean allMeta;
	
	
	/**
	 * Track if hierarchicalMode has been requested 
	 */
	protected boolean hierarchicalMode;
	
	/**
	 * Track if the request requested to include contexts 
	 */	
	protected boolean includeContexts;
	
	public BaseRequestInfo() {
		this(null, null);
	}
	
	public BaseRequestInfo(Integer offset, Integer limit) {
		this.offset = offset;
		this.limit = limit;
		this.includeMeta = false;
		this.allMeta = false;
		this.hierarchicalMode = false;
		this.includeContexts = false;
	}
	
	@Override
	public Integer getLimit() {
		return limit;
	}

	@Override
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Override
	public Integer getOffset() {
		return offset;
	}

	@Override
	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	@Override
	public boolean includeMeta() {
		return includeMeta;
	}

	@Override
	public void setIncludeMeta(boolean includeMeta) {
		this.includeMeta = includeMeta;
	}

	@Override
	public boolean allMeta() {
		return allMeta;
	}

	@Override
	public void setAllMeta(boolean allMeta) {
		this.allMeta = allMeta;
	}
	
	@Override
	public boolean isHierarchicalMode() {
		return hierarchicalMode;
	}
	
	@Override
	public void setHierarchicalMode(boolean hierarchicalMode) {
		this.hierarchicalMode = hierarchicalMode;
	}

	@Override
	public boolean includeContexts() {
		return includeContexts;
	}
	
	@Override
	public void setIncludeContexts(boolean includeContexts) {
		this.includeContexts = includeContexts;
	}

}
