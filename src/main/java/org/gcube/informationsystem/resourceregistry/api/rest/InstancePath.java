package org.gcube.informationsystem.resourceregistry.api.rest;

import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class InstancePath {
	
	public static final String INSTANCES_PATH_PART = "instances";
	
	public static final String OFFSET_QUERY_PARAMETER = "offset";
	public static final String LIMIT_QUERY_PARAMETER = "limit";
	
	public static final String POLYMORPHIC_QUERY_PARAMETER = "polymorphic";
	
	public static final String HIERARCHICAL_MODE_QUERY_PARAMETER = "hierarchical";
	
	public static final String INCLUDE_CONTEXTS_QUERY_PARAMETER = "includeContexts";
	
	/**
	 * Request to include {@link Metadata} in {@link IdentifiableElement} root instance
	 */
	public static final String INCLUDE_META_QUERY_PARAMETER = "includeMeta";
	
	/**
	 * Request to include {@link Metadata} in all {@link IdentifiableElement}
	 * instance.
	 * It must be used in conjunction with {@link #INCLUDE_META_QUERY_PARAMETER}
	 * If {@link #INCLUDE_META_QUERY_PARAMETER} is false it has no meaning
	 */
	public static final String INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER = "allMeta";
	
}
