package org.gcube.informationsystem.resourceregistry.api.rest.httputils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ExceptionMapper;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.RelationAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.RelationNotFoundException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class HTTPUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(HTTPUtility.class);
	
	protected static <C> NotFoundException getElementNotFoundException(Class<C> clz)
			throws NotFoundException, ResourceRegistryException {
		String error = String.format("Requested %s instance was not found", clz.getSimpleName());
		if(Resource.class.isAssignableFrom(clz)) {
			return new ResourceNotFoundException(error);
		} else if(Facet.class.isAssignableFrom(clz)) {
			return new FacetNotFoundException(error);
		} else if(Relation.class.isAssignableFrom(clz)) {
			return new RelationNotFoundException(error);
		}
		return new NotFoundException(error);
	}
	
	protected static <C> AvailableInAnotherContextException getElementAvailableInAnotherContextException(Class<C> clz) {
		String error = String.format("Requested %s instance was not found", clz.getSimpleName());
		if(Resource.class.isAssignableFrom(clz)) {
			return new ResourceAvailableInAnotherContextException(error);
		} else if(Facet.class.isAssignableFrom(clz)) {
			return new FacetAvailableInAnotherContextException(error);
		} else if(Relation.class.isAssignableFrom(clz)) {
			return new RelationAvailableInAnotherContextException(error);
		}
		return new AvailableInAnotherContextException(error);
	}
	
	protected static StringBuilder getStringBuilder(InputStream inputStream) throws IOException {
		StringBuilder result = new StringBuilder();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}
		}

		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <C> C getResponse(Class<C> clz, HttpURLConnection connection) throws Exception {
		
		try {
			
			int responseCode = connection.getResponseCode();
			String responseMessage = connection.getResponseMessage();
			
			if(connection.getRequestMethod().compareTo("HEAD")==0) {
				if(responseCode == HttpURLConnection.HTTP_NO_CONTENT) {
					return null;
				}
				if(responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
					throw getElementNotFoundException(clz);
				}
				if(responseCode == HttpURLConnection.HTTP_FORBIDDEN) {
					throw getElementAvailableInAnotherContextException(clz);
				}
			}
			
			if(responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
				
				InputStream inputStream = connection.getErrorStream();
				StringBuilder result = getStringBuilder(inputStream);
				
				String res = result.toString();
				
				ResourceRegistryException rre = null;
				try {
					rre = ExceptionMapper.unmarshal(ResourceRegistryException.class, res);
				} catch(Exception e) {
					rre = new ResourceRegistryException(responseMessage);
				}
				
				throw rre;
				
			}
			
			StringBuilder result = getStringBuilder(connection.getInputStream());
			
			String res = result.toString();
			logger.trace("Server returned content : {}", res);
			
			if(Boolean.class.isAssignableFrom(clz)) {
				return (C) ((Boolean) Boolean.valueOf(res));
			} else if(Element.class.isAssignableFrom(clz)) {
				return (C) ElementMapper.unmarshal((Class<Element>) clz, res);
			}
			
			return (C) res;
		} finally {
			connection.disconnect();
		}
	}
	
}
