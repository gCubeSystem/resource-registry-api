/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTemplateCreationException extends QueryTemplateException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 7777044293030289951L;

	public QueryTemplateCreationException(String message) {
		super(message);
	}
	
	public QueryTemplateCreationException(Throwable cause) {
		super(cause);
	}
	
	public QueryTemplateCreationException(String message, Throwable cause) {
		super(message, cause);
	}
}
