package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SharingPath {
	
	public static final String SHARING_PATH_PART = "sharing";
	
	public static final String CONTEXTS_PATH_PART = ContextPath.CONTEXTS_PATH_PART;
	
	public static final String OPERATION_QUERY_PARAMETER = "operation";
	
	public enum SharingOperation {
		ADD,
		REMOVE
	}
	
	public static final String DRY_RUN_QUERY_QUERY_PARAMETER = "dryRun";

	/**
	 * Used to add a resource to a context independently from the source
	 * context.
	 */
	public static final String FORCE_ADD_TO_CONTEXT_QUERY_PARAMETER = "forceAddToContext";
	
	public static final String INCLUDE_CONTEXTS_QUERY_PARAMETER = InstancePath.INCLUDE_CONTEXTS_QUERY_PARAMETER;
	public static final String INCLUDE_META_QUERY_PARAMETER = InstancePath.INCLUDE_META_QUERY_PARAMETER;
	public static final String INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER = InstancePath.INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER;
	
}
