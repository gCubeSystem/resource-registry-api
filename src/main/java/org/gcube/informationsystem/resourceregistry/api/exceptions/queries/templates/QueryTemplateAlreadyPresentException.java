package org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates;

import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTemplateAlreadyPresentException extends AlreadyPresentException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -3185950257529005913L;

	public QueryTemplateAlreadyPresentException(String message) {
		super(message);
	}

	public QueryTemplateAlreadyPresentException(Throwable cause) {
		super(cause);
	}

	public QueryTemplateAlreadyPresentException(String message, Throwable cause) {
		super(message, cause);
	}

}
