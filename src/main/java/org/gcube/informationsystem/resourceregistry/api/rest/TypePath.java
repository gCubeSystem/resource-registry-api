package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypePath {
	
	public static final String TYPES_PATH_PART = "types";
	
	public static final String POLYMORPHIC_QUERY_PARAMETER = InstancePath.POLYMORPHIC_QUERY_PARAMETER;
	
	public static final String INCLUDE_META_QUERY_PARAMETER = InstancePath.INCLUDE_META_QUERY_PARAMETER;
	
}