package org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTemplateException extends ResourceRegistryException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8119058749936021156L;

	public QueryTemplateException(String message) {
		super(message);
	}

	public QueryTemplateException(Throwable cause) {
		super(cause);
	}
	
	public QueryTemplateException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
