package org.gcube.informationsystem.resourceregistry.api.contexts;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.impl.relations.IsParentOfImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.tree.Tree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextCache {

	private static Logger logger = LoggerFactory.getLogger(ContextCache.class);
	
	// in millisec
	public static final long DEFAULT_EXPIRING_TIMEOUT;
	public static int expiringTimeout;
	
	static {
		DEFAULT_EXPIRING_TIMEOUT = TimeUnit.HOURS.toMillis(6);
		expiringTimeout = (int) DEFAULT_EXPIRING_TIMEOUT;
	}
	
	public static void setExpiringTimeout(int expiringTimeout) {
		ContextCache.expiringTimeout = expiringTimeout;
	}

	protected static ContextCache singleton;
	
	public synchronized static ContextCache getInstance() {
		if(singleton==null) {
			singleton = new ContextCache();
		}
		return singleton;
	}
	
	protected ContextCacheRenewal contextCacheRenewal;
	
	// in millisec used only for logging and debugging
	protected Calendar creationTime;
	// in millisec
	protected Calendar expiringTime;
	
	protected List<Context> contexts;
	protected Map<UUID, Context> uuidToContext;
	protected Map<UUID, String> uuidToContextFullName;
	protected Map<String, UUID> contextFullNameToUUID;
	protected Tree<Context> contextsTree;
	
	protected boolean initialized;
	
	public ContextCache() {
		Calendar now = Calendar.getInstance();
		cleanCache(now);
		initialized = false;
	}
	
	public void cleanCache() {
		cleanCache(Calendar.getInstance());
	}
	
	protected void cleanCache(Calendar calendar) {
		this.contexts = new ArrayList<>();
		this.uuidToContext = new LinkedHashMap<>();
		this.uuidToContextFullName = new LinkedHashMap<>();
		this.contextFullNameToUUID = new TreeMap<>();
		this.contextsTree = new Tree<Context>(new ContextInformation());
		this.contextsTree.setAllowMultipleInheritance(false);
		this.creationTime = Calendar.getInstance();
		this.creationTime.setTimeInMillis(calendar.getTimeInMillis());
		this.expiringTime = Calendar.getInstance();
		this.expiringTime.setTimeInMillis(calendar.getTimeInMillis());
		this.expiringTime.add(Calendar.MILLISECOND, -1);
		this.expiringTime.add(Calendar.MILLISECOND, expiringTimeout);
		initialized = false;
	}
	
	public void renew() throws ResourceRegistryException {
		cleanCache();
		refreshContextsIfNeeded();
	}
	
	public ContextCacheRenewal getContextCacheRenewal() {
		return contextCacheRenewal;
	}

	public void setContextCacheRenewal(ContextCacheRenewal contextCacheRenewal) {
		if(this.contextCacheRenewal==null) {
			this.contextCacheRenewal = contextCacheRenewal;
		}
	}
	
	public synchronized void refreshContextsIfNeeded() throws ResourceRegistryException {
		Calendar now = Calendar.getInstance();
		if((now.after(expiringTime) || (!initialized)) && contextCacheRenewal!=null) {
			try {
				List<Context> contexts = contextCacheRenewal.renew();
				setContexts(now, contexts);
				initialized = true;
			} catch (ResourceRegistryException  e) {
				if(!initialized) {
					logger.error("Unable to initialize Context Cache", e);
					throw e;
				}else {
					logger.error("Unable to refresh Context Cache", e);
				}
			}
			
		}
	}
	
	public synchronized List<Context> getContexts() throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return contexts;
	}

	public void setContexts(List<Context> contexts) {
		Calendar now = Calendar.getInstance();
		setContexts(now, contexts);
	}
	
	protected void setContexts(Calendar calendar, List<Context> contexts) {
		cleanCache(calendar);
		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			Context context = new ContextImpl(c.getName());
			context.setMetadata(c.getMetadata());
			context.setID(uuid);
			this.contexts.add(context);
			this.uuidToContext.put(uuid, context);
		}
		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			Context context = this.uuidToContext.get(uuid);
			if(c.getParent()!=null) {
				IsParentOf ipo = c.getParent();
				UUID parentUUID = ipo.getSource().getID();
				Context parent = this.uuidToContext.get(parentUUID);
				IsParentOf isParentOf = new IsParentOfImpl(parent, context);
				isParentOf.setID(parentUUID);
				isParentOf.setMetadata(ipo.getMetadata());
				parent.addChild(isParentOf);
				context.setParent(isParentOf);
			}
		}
		
		for(Context context : contexts) {
			UUID uuid = context.getID();
			String fullName = getContextFullName(context);
			this.uuidToContextFullName.put(uuid, fullName);
			this.contextFullNameToUUID.put(fullName, uuid);
		}
		
		SortedSet<String> contextFullNames = new TreeSet<String>(contextFullNameToUUID.keySet());
		for(String contextFullName : contextFullNames) {
			UUID uuid = contextFullNameToUUID.get(contextFullName);
			Context context = uuidToContext.get(uuid);
			contextsTree.addNode(context);
		}
		
	}

	protected String getContextFullName(Context context) {
		StringBuilder stringBuilder = new StringBuilder();
		IsParentOf ipo = context.getParent();
		if(ipo!=null) {
			Context c = ipo.getSource();
			c = uuidToContext.get(c.getID());
			String parentFullName = getContextFullName(c);
			stringBuilder.append(parentFullName);
		}
		stringBuilder.append("/");
		stringBuilder.append(context.getName());
		return stringBuilder.toString();
	}
	
	
	public synchronized String getContextFullNameByUUID(UUID uuid) throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return uuidToContextFullName.get(uuid);		
	}
	
	public synchronized String getContextFullNameByUUID(String uuid) throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return uuidToContextFullName.get(UUID.fromString(uuid));		
	}
	
	public synchronized UUID getUUIDByFullName(String contextFullName) throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return contextFullNameToUUID.get(contextFullName);
	}
	
	public synchronized Context getContextByUUID(UUID uuid) throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return uuidToContext.get(uuid);
	}
	
	public synchronized Context getContextByUUID(String uuid) throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return getContextByUUID(UUID.fromString(uuid));
	}
	
	public synchronized Context getContextByFullName(String contextFullName) throws ResourceRegistryException {
		UUID uuid = getUUIDByFullName(contextFullName);
		return getContextByUUID(uuid);
	}
	
	/**
	 * @return an Map containing UUID to Context FullName association
	 */
	public synchronized Map<UUID, String> getUUIDToContextFullNameAssociation() throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return new LinkedHashMap<>(uuidToContextFullName);
	}
	
	/**
	 * @return an Map containing Context FullName to UUID association
	 */
	public synchronized Map<String, UUID> getContextFullNameToUUIDAssociation() throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return new TreeMap<>(contextFullNameToUUID);
	}
	
	public Tree<Context> getContextsTree() throws ResourceRegistryException {
		refreshContextsIfNeeded();
		return contextsTree;
	}
	
}
