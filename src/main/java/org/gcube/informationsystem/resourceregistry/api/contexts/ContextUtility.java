package org.gcube.informationsystem.resourceregistry.api.contexts;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonParseException;
import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextUtility {
	
	private static Logger logger = LoggerFactory.getLogger(ContextUtility.class);

//	public static Set<UUID> getContextUUIDSet(String jsonArray) throws IOException {
//		ObjectMapper mapper = ElementMapper.getObjectMapper();
//		JavaType type = mapper.getTypeFactory().constructCollectionType(HashSet.class, UUID.class);
//		return mapper.readValue(jsonArray, type);
//	}
//	
//	public static Set<String> getContextFullNameSet(String jsonArray) throws Exception {
//		Set<UUID> uuids = getContextUUIDSet(jsonArray);
//		return getContextFullNameSet(uuids);
//	}
	
	public static Map<UUID, String> getContextMap(String objectnode) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = ElementMapper.getObjectMapper();
		JavaType type = mapper.getTypeFactory().constructMapType(HashMap.class, UUID.class, String.class);
		return mapper.readValue(objectnode, type);
	}
	
	public static Set<UUID> getContextUUIDSet(Collection<String> uuidStrings) throws Exception {
		Set<UUID> uuids = new HashSet<>();
		for(String uuidString : uuidStrings) {
			UUID uuid = UUID.fromString(uuidString);
			uuids.add(uuid);
		}
		return uuids;
	}
	
	public static Set<String> getContextFullNameSet(ContextCache contextCache, Collection<UUID> uuids) throws Exception {
		Set<String> contextFullNames = new HashSet<>();
		try {
			Map<UUID, String> uuidToContextFullNameAssociation = contextCache.getUUIDToContextFullNameAssociation();
			if(!uuidToContextFullNameAssociation.keySet().containsAll(uuids)) {
				logger.debug("{} does not contain all the contexts identified by the following UUID list {}. Trying to invalidate the cache", ContextCache.class.getSimpleName(), uuids);
				contextCache.cleanCache();
				uuidToContextFullNameAssociation = contextCache.getUUIDToContextFullNameAssociation();
			}
			for(UUID uuid : uuids) {
				String fullName = uuidToContextFullNameAssociation.get(uuid);
				if(fullName==null) {
					throw new Exception("Unable to get the full name of context with UUID " + uuid.toString());
				}
				contextFullNames.add(fullName);
			}
		}catch (Exception e) {
			throw new Exception("Unable to convert Set<UUID> to Set<String> containing Context Full Name. Be sure the cache has been configured to retrieve the context list or provide a valid UUID list", e);
		}
		return contextFullNames;
	}
	
}
