package org.gcube.informationsystem.resourceregistry.api.contexts;

import java.util.LinkedHashSet;
import java.util.Set;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.tree.NodeInformation;

public class ContextInformation implements NodeInformation<Context> {

	@Override
	public String getIdentifier(Context context) {
		return context.getID().toString();
	}

	@Override
	public Set<String> getParentIdentifiers(Context root, Context context) {
		Set<String> set = new LinkedHashSet<>();
		if(root !=null && context.getID().compareTo(root.getID())==0) {
			return set;
		}
		IsParentOf parent = context.getParent();
		if(parent!=null) {
			set.add(parent.getSource().getID().toString());
		}
		return set;
	}

}
