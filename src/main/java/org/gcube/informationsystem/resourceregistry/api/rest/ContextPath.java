package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextPath {
	
	public static final String CONTEXTS_PATH_PART = "contexts";

	public static final String CURRENT_CONTEXT_PATH_PART = "CURRENT_CONTEXT";
	
	public static final String INCLUDE_META_QUERY_PARAMETER = InstancePath.INCLUDE_META_QUERY_PARAMETER;
	public static final String OFFSET_QUERY_PARAMETER = InstancePath.OFFSET_QUERY_PARAMETER;
	public static final String LIMIT_QUERY_PARAMETER = InstancePath.LIMIT_QUERY_PARAMETER;
	
}
