package org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates;

import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTemplateNotFoundException extends NotFoundException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3034336911176161784L;

	public QueryTemplateNotFoundException(String message) {
		super(message);
	}

	public QueryTemplateNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public QueryTemplateNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
