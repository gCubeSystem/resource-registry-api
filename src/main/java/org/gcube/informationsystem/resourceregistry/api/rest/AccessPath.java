package org.gcube.informationsystem.resourceregistry.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AccessPath {
	
	public static final String ACCESS_PATH_PART = "access";
	public static final String CONTEXTS_PATH_PART = ContextPath.CONTEXTS_PATH_PART;
	public static final String TYPES_PATH_PART = TypePath.TYPES_PATH_PART;
	public static final String INSTANCES_PATH_PART = InstancePath.INSTANCES_PATH_PART;
	public static final String QUERY_TEMPLATES_PATH_PART = QueryTemplatePath.QUERY_TEMPLATES_PATH_PART;
	
	public static final String QUERY_PATH_PART = "query";
	
	public static final String Q_QUERY_PARAMETER = "q";
	public static final String RAW_QUERY_PARAMETER = "raw";
	public static final String RAW_QUERY_PARAMETER_DEFAULT_VALUE = "false";
	
	public static final String HIERARCHICAL_MODE_QUERY_PARAMETER = InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER;
	public static final String INCLUDE_CONTEXTS_QUERY_PARAMETER = InstancePath.INCLUDE_CONTEXTS_QUERY_PARAMETER;
	public static final String INCLUDE_META_QUERY_PARAMETER = InstancePath.INCLUDE_META_QUERY_PARAMETER;
	public static final String INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER = InstancePath.INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER;
	
	public static final String OFFSET_QUERY_PARAMETER = InstancePath.OFFSET_QUERY_PARAMETER;
	public static final String LIMIT_QUERY_PARAMETER = InstancePath.LIMIT_QUERY_PARAMETER;
	
	
	/**
	 * Used only in getRelated() function
	 * The _ is prepended to avoid clash with field name 
	 */
	public static final String _REFERENCE_QUERY_PARAMETER = "_reference";
	/**
	 * Used only in getRelated() function
	 * The _ is prepended to avoid clash with field name 
	 */
	public static final String _POLYMORPHIC_QUERY_PARAMETER = "_polymorphic";
	/**
	 * Used only in getRelated() function
	 * The _ is prepended to avoid clash with field name 
	 */
	public static final String _DIRECTION_QUERY_PARAMETER = "_direction";
	
}
